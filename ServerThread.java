/*
Modifichiamo ora quanto appreso in cod_1 ed implementiamo
un Server versione multithreaded, ovvero un server in grado di
più richieste (per esempio quelle di 10 client che inviano ciascuno 10 messaggi)
in maniera concorrente.

Per semplicità eliminiamo l’interazione con la console e utilizziamo client che
inviano messaggi “automatici".

Per far ciò ci serviremo della classe Thread. Esso è una suddivisione
di un processo in due o più filoni o sottoprocessi che vengono eseguiti
concorrentemente da un sistema di elaborazione.

Il server dovrà ciclare all’infinito in attesa di connessioni;
ogni volta che riceve una nuova richiesta di connessione da parte di un client,
dovrà generare un thread che si occuperà di quella specifica richiesta,
dopodichè il server si rimetterà in attesa di un’altra richiesta e ripeterà l’operazione.
 */

package cod_4;

import java.io.*;
import java.net.*;

class ServerThread extends Thread {

    private static int counter = 0;
    private int id = ++counter; //prima incremento, poi assegno (l'ho distinto da client per non dimenticarsi questo dettaglio)
    //Supponiamo che ogni Thread sia identificato da un id univoco (ricavato incrementalmente)

    private Socket client;

    //Classi per la lettura e scrittura di stringhe nello stream
    private BufferedReader in;
    private PrintWriter out;
    //NOTA: per evitare errori, utilizzare le stesse classi anche a lato client.

    //********** COSTRUTTORE ************
    public ServerThread(Socket s) throws IOException
    {
        client = s;

        in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        //specifico alla classe BufferedReader che ho bisogno di ricevere il flusso di input del socket client

        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream())), true);
        /*
        autoflush mi permette di stampare (mostrare a video) in ordine.
        Detto in parole semplici, la variabile out invia ordinatamente ciò che
        deve inviare e mostrerà tutto il contenuto prima di mostrare ciò che hanno
        eventuali altri thread.
        */

        System.out.println("ServerThread " + id + ": started\n");

        //Metodo della classe Thread
        start();
        /*
        Chiama il metodo run() sull’oggetto/thread t della classe
        mioThread che abbiamo implementato. Il thread t termina
        quando il metodo run() ha finito l’esecuzione.
        Un thread non può essere fatto ripartire, cioè il metodo
        start() deve essere chiamato solo una volta, altrimenti viene
        generata una InvalidThreadStateException.
         */
    }

    /*
    Thread è una classe NON astratta che fornisce vari metodi
    per controllare l'esecuzione di un thread (start(), isAlive(), interrupt())
    Tuttavia, una sottoclasse Thread DEVE implementare il metodo run()!
    */
    public void run()
    {
        try {
            while (true)
            {
                String response = "";
                String req_client = in.readLine(); // request client: prendo nel mio flusso in input ciò che proviene dal client.
                String client_req = "Client req: " + req_client;

                if (req_client.equals("END") || req_client.equals("end")) {
                    System.out.println(client_req);
                    break;
                }

                else
                    response = req_client.toUpperCase();
                // Supponiamo che il server offra il servizio di risponder ai messaggi in maiuscolo
                System.out.println(client_req);
                System.out.println("ServerThread " + id + ": echoing -> " + response);

                out.println(response); //nel flusso in uscita del server invio il messaggio di risposta
            }

            System.out.println("\nServerThread " + id + ": closing...");
        }
        catch (IOException e) {
            e.printStackTrace(System.err);
            //Se ho ottenuto un errori di I/O relativo non chiudo termino lo scorrere del flusso
            //di forza bruta ma bensì mostrerò l'errore corrispondente e dopodichè
            //chiuderò il canale di comunicazione (la socket del client) e i flussi di I/O.
            System.err.println(e.getMessage());
        }

        finally {
            try { //DA NON DIMENTICARE MAI!
                client.close();
                in.close();
                out.close();

                System.out.println("ServerThread " + id + " closed successfully!");
                System.out.println("--------------------------------------------");
            } catch (Exception e) {
                System.err.println("Errore durante la chiusura del client " + id);
                e.printStackTrace(System.err);
            }
        }
    }

}