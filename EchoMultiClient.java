package cod_4;

import java.net.*;
import java.io.*;

public class EchoMultiClient {

    private static final int MAX_THREADS = 10;

    public static void main(String[] args) throws IOException,InterruptedException
    {
        /*
        Tramite le impostazioni di Run del proprio IDE, poniamo il nome
        del nostro host (della nostra "macchina", nel mio caso mbp-nj trovato tramite
        l'esercizio cod_1). È un nome che solitamente poniamo noi durante la prima
        accensione del SO.

        Dunque, tramite la classe InetAddress andremmo a ricavare tutte le informazioni
        del nostro host (indirizzo ip, dns etc...).
         */

        InetAddress addr; //indirizzo nostro host

        if (args.length !=1) //In quest'esercizio dobbiamo porne solo uno.
            addr = InetAddress.getByName(null);
        else
            addr = InetAddress.getByName(args[0]);


        //semplice ciclo di creazione di Client fino ad un nr massimo prestabilito.
        while(true && ClientThread.threadCount() <= MAX_THREADS)
        {
            try {
                new ClientThread(addr);

                Thread.currentThread().sleep(1000);
                // Il thread sarà pianificato per l'esecuzione
                // dal SO per la quantità di tempo specificato.
            }
            catch(Exception e){
                System.err.println("Errore durante la creazione di un client.");
                e.printStackTrace(System.err);
            }
        }
        /*
        Una volta che i 10 client "sono stati eseguiti",
        il programma termina. Ma ci stiamo riferendo, in questo caso,
        solo a 10 thread eseguiti dalla macchina su cui ci troviamo.

        NB: Per lasciare "accesso" il programma multiclient porre while(true).
         */
    }
}
