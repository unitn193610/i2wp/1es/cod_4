package cod_4;

import java.net.*;
import java.io.*;

public class EchoMultiServer {

    static final int PORT = 3000;

    public static void main(String[] args) throws IOException {
        /*
         Ricorda che la classe IOException è la classe generale di eccezioni prodotte
         da operazioni di I / O non riuscite, interrotte o non interpretate.
         Si noti che questo non verrà generato per la lettura o la scrittura in
         memoria poiché Java lo gestirà automaticamente.
         Alcuni casi che generano IOException sono:
         • Lettura da un inputstream chiuso
         • Prova ad accedere a un file su Internet senza una connessione di rete.
         */
        System.out.println("EchoMultiServer: started\n");

        ServerSocket serverSocket = new ServerSocket(PORT);
        //Ricordiamo che ServerSocket è la classe per la gestione di socket lato server
        System.out.println("Server Socket: " + serverSocket.toString());

        try{
            while(true)
            {
                // Bloccante finchè non avviene una connessione.
                Socket clientSocket = serverSocket.accept();
                // NOTA: in questo caso devo istanziare un nuovo clientSocket ad ogni connessione.
                // NON posso sovrascrivere un thread già in esecuzione!! Pena errore/interruzione.

                System.out.println("Connection accepted. ");
                System.out.println("Client Socket: " + clientSocket.toString());

                try {
                    new ServerThread(clientSocket);
                    /*
                    Detto in modo informale, il server apre un thread in cui
                    ci sarà un canale di comunicazione con il client passato
                    come argomento.
                     */
                }
                catch (Exception e) {
                    e.printStackTrace(System.err);
                    System.err.println("Errore durante la creazione di un nuovo Client-Socket.");

                    clientSocket.close();
                    /*
                    Quando ho accettato la connesione, subito dopo apro un nuovo serverThread.
                    Ebbene, nel caso si presentasse un qualsiasi errore (Exception)
                    chiudo la connessione accettata.
                    Nel caso vada tutto a buon fine, il server, e dunque il singolo
                    ServerThread(.java) procederà con la risposta al servizio richiesto
                    dal client e chiuderà il client. Detto in modo informale,
                    chiudiamo la clientSocket nell'altra codice (ServerThread.java).
                     */
                }
            }
        } catch (IOException e) {
            System.err.println("Accept failed");
            System.exit(1);
            /*
            In questa sezione, diversamente dal catch precedente, verifico la presenza
            di eventuali errori durante l'accettazione della connessione da parte del server con un client.
            Immaginiamo che ci sia un sovraccarico, ebbene in tal caso il server potrebbe presentare
            diversi stati di sistema che, nel nostro caso, consiste nello spegnimento del server (System.exit).
             */
        }

        /*
        La righe seguenti non verranno mai raggiunte perchè abbiamo fatto in modo
        che il server rimanga in ascolto continuamente.
        Infatti, facendo ripartire il codice "EchoMultiClient" noteremo
        che il server continuerà ad aumentare il suo contatore, ovvero
        continuerà ad aprire i suoi thread.
        Possiamo dunque provare ad eseguire altri Client indipendenti tra cui ad esempio
        il client dell'esercizio cod_1. Noteremo infatti il contatore di EchoMultiServer
        incrementerà solo di uno i suoi thread.

        Consiglio:
        Prova ad eseguire EchoMultiServer.java e successivamente EchoMultiClient.java.
        Noterai nell'output di entrambi la stampa dei loro messaggi (10) più
        la chiusura di tutti i thred. Il server però rimarrà ancora in esecuzione.
        Infatti, come detto precedentemente possiamo richiedere il servizio di questo server
        tramite un altro client indipendente. Tenendo dunque EchoMultiServer.java in esecuzione,
        proviamo anche a far partire Client.java della cartella cod_1 e inviamo il messaggio: "end".
        Noteremo che il server chiuderà la socket aperta per il client in questione e altrettanto
        il client.java.
         */

        System.out.println("EchoMultiServer: closing...");
        serverSocket.close();
        /*
        Alternativamente potremmo porre delle determinate condizioni, a nostro piacimento,
        per la chiusura del server (che nella realtà può avvenire ad esempio in caso di
        guasti, manutenzioni, cyber-attacchi, etc...).
        Ad esempio, come descritto precedentemente, possiamo porre un numero massimo
        di threads che il server può avere (simuliamo dunque un sovraccarico).
        Nel caso tale limite venisse raggiunto, piuttosto che "arrestare" di forza bruta il server
        (vedi quanto descritto nel caso precedente tramite le condizioni dettate dal System.exit(1) dell'ultimo catch)
        possiamo chiudere il serverSocket anticipatamente e quindi, oltre a mandare determinati
        messaggi di risposta ai client, prevenire perdite di dati e gestire tale sovraccarico
        (ad esempio inoltrando le richieste verso un altro server).
         */
    }
}