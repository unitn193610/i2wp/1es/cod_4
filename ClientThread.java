package cod_4;

import java.io.*;
import java.net.*;

class ClientThread extends Thread {

    private Socket client;

    //Classi per la lettura e scrittura di stringhe nello stream
    private BufferedReader in;
    private PrintWriter out;
    //NOTA: per evitare errori, utilizzare le stesse classi anche a lato server.

    private static int counter = 1;
    private int id = counter++; //prima si assegna l'id, poi si incrementa la variabile counter
    private static int threadcount = 1;
    //potrei utilizzare il counter per contare i thread però poi non potrò,
    //eventualmente, creare id univoci in un altra maniera essendo vincolato al counter.

    public static int threadCount() {
        return threadcount;
    }

    // *********** COSTRUTTORE **********
    public ClientThread(InetAddress addr) {

        threadcount++; //ricorda, primo assegno poi incremento.

        try {
            client = new Socket(addr, EchoMultiServer.PORT);

            System.out.println("\n-------------------------------");
            System.out.println("EchoClient n° " + id + ": started");
            System.out.println("Client " + client.toString() + "\n");
        }
        catch (UnknownHostException e){
            System.err.println("Host sconosciuto!");
            e.printStackTrace();
        }
        catch(Exception e) {
            // Se la creazione della socket fallisce non è necessario fare nulla perchè
            // ci occupiamo di questo errore nella parte di codice "EchoMultiClient.java"
            System.err.println("Errore durante la creazione di un nuovo socket: " + e.getMessage());
        }

        try {
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream())), true);

            start();
            /*
            Chiama il metodo run() sull’oggetto/thread t della classe
            mioThread che abbiamo implementato. Il thread t termina
            quando il metodo run() ha finito l’esecuzione.
            Un thread non può essere fatto ri-partire, cioè il metodo
            start() deve essere chiamato solo una volta, altrimenti viene
            generata una InvalidThreadStateException.
             */
        }
        catch(Exception e1) {
            // in seguito ad ogni fallimento la socket deve essere chiusa, altrimenti
            // verrà chiusa dal metodo run() del thread
            try {
                System.out.println("Client " + id + " closed!");
                client.close();
            }
            catch(Exception e) {
                System.err.println("Errore anche durante la chiusura.");
                e.printStackTrace(System.err);
            }
        }
    }

    public void run() {
        try {
            for (int i = 1; i < 10; i++) {
                String my_req = "client " + id + " msg " + i;
                System.out.println("Msg request: " + my_req);
                out.println(my_req);//out.println(my_req); //il client manda nel suo flusso in uscita ciò che richiederà al server (request).

                String res_server = in.readLine(); //leggo la risposta del server ricevuta nel mio flusso in entrata
                System.out.println("Msg response: " + res_server);
            }

            System.out.println("Msg request: client " + id + " MSG END (10)");
            out.println("END");//out.println("END"); //l'ultimo messaggio, il 10mo, dirà al server che ha terminato il suo servizio.
        }
        catch (IOException e) {
            e.printStackTrace(System.err);
            System.err.println("Errore durante l'invio/ricezione di un messaggio.");
        }

        try {
            System.out.println("\nClient " + id + " closing...");

            client.close();
            in.close();
            out.close();

            System.out.println("Client "+ id + " closed successfully!");
        }
        catch (IOException e) {
            System.err.println("Errore durante la chiusura del client " + id);
            e.printStackTrace(System.err);
            //threadcount--; //questa riga non l'ho capita
        }
    }
}